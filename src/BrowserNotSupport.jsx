import React from "react";
import zeevou_logo from './assets/zeevou_logo.png';
import browser_not_support from './assets/browser_not_support.png';
import "./style.css";

const BrowserNotSupport = () => {
  return (
    <div className="browser-not-support-template">
      <div className="zeevou-logo-box">
        <img src={zeevou_logo} alt="zeevou_logo" />
      </div>
      <div className="browser-not-support-image-box">
        <img src={browser_not_support} alt="zeevou_logo" width="365px" height="312px" />
      </div>
      <div className="error-text-box">
        <h4>Time to move on...</h4>
        <p>
          This page does not work on your current browser.
          <br/>
          Please download & install <a href="https://www.google.com/chrome/">google chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">fire fox</a> and come back to us :)
        </p>
      </div>
    </div>
  );
};

export default BrowserNotSupport;
