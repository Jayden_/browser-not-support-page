import React from 'react';
import ReactDOM from 'react-dom';
import BrowserNotSupport from './BrowserNotSupport';

ReactDOM.render(<BrowserNotSupport />, document.querySelector("#root"));